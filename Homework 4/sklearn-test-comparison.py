from sklearn.naive_bayes import MultinomialNB
from sklearn.feature_extraction.text import CountVectorizer
from sklearn import metrics
import csv

training_x = []
with open('training_x.csv', 'r') as f:
	reader = csv.reader(f)
	training_x = list(reader)

training_y = []
with open('training_y.csv', 'r') as f:
	for line in f:
		training_y.append(int(line))

dev_x = []
with open('dev_x.csv', 'r') as f:
	reader = csv.reader(f)
	dev_x = list(reader)

dev_y = []
with open('dev_y.csv', 'r') as f:
	for line in f:
		dev_y.append(int(line))

vectorizer = CountVectorizer(stop_words='english')
training_x = vectorizer.fit_transform([r[0] for r in training_x])
dev_x = vectorizer.transform([r[0] for r in dev_x])

nb = MultinomialNB()
nb.fit(training_x, [int(r) for r in training_y])

print("Accuracy: ", str(nb.score(dev_x, dev_y) *100), "%")