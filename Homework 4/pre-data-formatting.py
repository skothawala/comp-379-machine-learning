import numpy as np
import pandas as pd
import re


def getData():
	toReturn = []
	with open('combined_data.csv', 'r') as f:
		
		for line in f:			
			toReturn.append(re.sub("[^\w]", " ",  line).split())
			toReturn[-1][-1] = int(toReturn[-1][-1])
	return toReturn

data = np.asarray(getData())
np.random.shuffle(data)

endIndexTrainingData = round(len(data)*.7)
endIndexDevData = endIndexTrainingData + round(len(data)*.15)

trainingDataSet = data[:endIndexTrainingData]
f = open('training_x.csv', 'w')
for line in trainingDataSet:
	f.write(",".join(line[:-1]) + "\n")
f.close()

f = open('training_x_and_y.csv', 'w')
for line in trainingDataSet:
	f.write(",".join(str(l) for l in line) + "\n")
f.close()


f = open('training_y.csv', 'w')
for line in trainingDataSet:
	f.write(str(line[-1]) + "\n")
f.close()

devDataSet = data[endIndexTrainingData:endIndexDevData]
f = open('dev_x.csv', 'w')
for line in devDataSet:
	f.write(",".join(line[:-1]) + "\n")
f.close()

f = open('dev_y.csv', 'w')
for line in devDataSet:
	f.write(str(line[-1]) + "\n")
f.close()

f = open('dev_x_and_y.csv', 'w')
for line in devDataSet:
	f.write(",".join(str(l) for l in line) + "\n")
f.close()

testDataSet = data[endIndexDevData:]
f = open('test_x.csv', 'w')
for line in testDataSet:
	f.write(",".join(line[:-1]) + "\n")
f.close()

f = open('test_y.csv', 'w')
for line in testDataSet:
	f.write(str(line[-1]) + "\n")
f.close()

f = open('test_x_and_y.csv', 'w')
for line in testDataSet:
	f.write(",".join(str(l) for l in line) + "\n")
f.close()

