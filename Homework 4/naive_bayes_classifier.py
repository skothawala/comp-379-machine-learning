import csv
from collections import Counter
import numpy as np

class NaiveBayesImpl:
	
	def __init__(self, X, Y):
		self.dictionary = {}
		self.classifications = {}
		self.total_training_examples = 0

		stop_words = ["a", "about", "above", "after", "again", "against", "all", "am", "an", "and", "any", "are", "aren't", "as", "at", "be", "because", "been", "before", "being", "below", "between", "both", "but", "by", "can't", "cannot", "could", "couldn't", "did", "didn't", "do", "does", "doesn't", "doing", "don't", "down", "during", "each", "few", "for", "from", "further", "had", "hadn't", "has", "hasn't", "have", "haven't", "having", "he", "he'd", "he'll", "he's", "her", "here", "here's", "hers", "herself", "him", "himself", "his", "how", "how's", "i", "i'd", "i'll", "i'm", "i've", "if", "in", "into", "is", "isn't", "it", "it's", "its", "itself", "let's", "me", "more", "most", "mustn't", "my", "myself", "no", "nor", "not", "of", "off", "on", "once", "only", "or", "other", "ought", "our", "ours", "ourselves", "out", "over", "own", "same", "shan't", "she", "she'd", "she'll", "she's", "should", "shouldn't", "so", "some", "such", "than", "that", "that's", "the", "their", "theirs", "them", "themselves", "then", "there", "there's", "these", "they", "they'd", "they'll", "they're", "they've", "this", "those", "through", "to", "too", "under", "until", "up", "very", "was", "wasn't", "we", "we'd", "we'll", "we're", "we've", "were", "weren't", "what", "what's", "when", "when's", "where", "where's", "which", "while", "who", "who's", "whom", "why", "why's", "with", "won't", "would", "wouldn't", "you", "you'd", "you'll", "you're", "you've", "your", "yours", "yourself", "yourselves", "the", "film", "movie", "picture"]

		if len(X) != len(Y):
			raise Exception("Lenghts of X and Y don't match")

		for idx, sentence in enumerate(X):
			sentence = Counter(sentence).most_common(len(sentence))
			wordsToTrain = []
			for wordCountPair in sentence:
				if wordCountPair[0] not in stop_words and len(wordCountPair[0]) > 2:
					#self.train(word, Y[idx])
					wordsToTrain.append(wordCountPair[0])
				if len(wordsToTrain) > 9:
					break

			for wordToTrain in wordsToTrain:
				self.train(wordToTrain, Y[idx])

			self.total_training_examples += 1
			if Y[idx] in self.classifications:
				self.classifications[Y[idx]] += 1
			else:
				self.classifications[Y[idx]] = 1

	def train(self, word, classification):
		if word in self.dictionary:			
			self.dictionary[word]["count"] += 1

			if classification in self.dictionary[word]["classifications"]:
				self.dictionary[word]["classifications"][classification] += 1
			else:
				self.dictionary[word]["classifications"][classification] = 1
		else:
			self.dictionary[word] = {"count": 1, "classifications": {str(classification): 1}}

	def predict(self, review):
		probability_good = np.log(self.classifications[1] / self.total_training_examples)
		probability_bad = np.log(self.classifications[0] / self.total_training_examples)
		

		review = Counter(review).most_common(len(review))
		
		for wordCountPair in review:
			word = wordCountPair[0]
			if word in self.dictionary:
				if "1" in self.dictionary[word]["classifications"]:
					probability_good += np.log((self.dictionary[word]["classifications"]["1"] + 1) / (self.classifications[1] + len(set(self.dictionary))))
				else:
					probability_good += np.log((1) / (self.classifications[1] + len(set(self.dictionary))))
				
				if "0" in self.dictionary[word]["classifications"]:
					probability_bad += np.log((self.dictionary[word]["classifications"]["0"] + 1) / (self.classifications[0] + len(set(self.dictionary))))
				else:
					probability_bad += np.log((1) / (self.classifications[0] + len(set(self.dictionary))))
				
		#print("Probaility good: ", probability_good, "Probaility Bad:", probability_bad)
		if probability_good > probability_bad:
			return 1
		else:
			return 0

training_x = []
with open('training_x.csv', 'r') as f:
	reader = csv.reader(f)
	training_x = list(reader)

training_y = []
with open('training_y.csv', 'r') as f:
	for line in f:
		training_y.append(int(line))


n = NaiveBayesImpl(training_x, training_y)

dev_x = []
with open('dev_x.csv', 'r') as f:
	reader = csv.reader(f)
	dev_x = list(reader)

dev_y = []
with open('dev_y.csv', 'r') as f:
	for line in f:
		dev_y.append(int(line))

numberRight = 0
for idx, toPredict in enumerate(dev_x):
    prediction = n.predict(toPredict)
    if prediction == dev_y[idx]:
        numberRight += 1

accuracy = numberRight / len(dev_y) * 100
print("Dev Accuracy: ", str(accuracy), "%")



test_x = []
with open('test_x.csv', 'r') as f:
	reader = csv.reader(f)
	test_x = list(reader)

test_y = []
with open('test_y.csv', 'r') as f:
	for line in f:
		test_y.append(int(line))



numberRight = 0
for idx, toPredict in enumerate(test_x):
    prediction = n.predict(toPredict)
    if prediction == test_y[idx]:
        numberRight += 1

accuracy = numberRight / len(dev_y) * 100
print("Test (final) Accuracy: ", str(accuracy), "%")













