import numpy as np
import sys, re, os
from scipy import stats
from functools import reduce

assert sys.version_info >= (3,0) #Running with 2.xx does work, but gives WEIRD errors

"""
Returns an array with the list of passengers and their attributes

Params:
`fileLocation` - location of the file to read from
`isTraining` - if true, then the survivor column is empty and indexes need to be moved down one
"""    
def getData(fileLocation, isTraining = True):

    passengers = [] #to return
    with open(fileLocation, 'r') as f:
        f.readline() #get rid of headers
        for line in f:
            line = re.sub(r'(\"(.*),(.*)\")', r"\3 \2", line) #format lName, fName -> fName lName
            line = line[:-1].split(',') #get rid of \n and explode to csv

            passenger = np.empty((13 if isTraining else 12), dtype=object) #create holder array

            for index, passengerAttribute in enumerate(line):
                if index is (4 if isTraining else 3):
                    if passengerAttribute ==  'male':
                        passenger[index] = 0
                    else:
                        passenger[index] = 1
                elif index is (5 if isTraining else 4):
                    #some ages are to .5's
                    if passengerAttribute is not '':
                        passenger[index] = float(passengerAttribute)
                    else:
                        passenger[index] = -1
                elif index in ([0, 1, 2, 6, 7] if isTraining else [0, 1, 5, 6]):
                    #these are boolean values we might want to run correlations on
                    if passengerAttribute is not '':
                        passenger[index] = int(passengerAttribute)
                elif index is (10 if isTraining else 9): #cabin if present
                    if passengerAttribute is not '':
                        passenger[index] = ord(passengerAttribute[0])
                    else:
                        passenger[index] = -1
                else:
                    passenger[index] = passengerAttribute
            
            passengers.append(passenger)             
    return passengers;

"""
Returns a list of dictionaries. Each index represents a feature that we can analayze.
The dictionary at that index contains all the possible values and the amount of passengers with that value.


Params:
`data` - list of passengers and their attributes
"""   
def getListOfAttributesAndNumberOfPeopleWithThatAttribute(data):
    MASTER_DICT = np.empty(13, dtype=object) #TODO: rename this

    women = np.where(data[:,4] == 1)[0]
    men = np.where(data[:,4] == 0)[0]

    MASTER_DICT[4] = {
        1: women,
        0: men
    }

    first_class = np.where(data[:,2] == 1)[0]
    second_class = np.where(data[:,2] == 2)[0]
    third_class = np.where(data[:,2] == 3)[0]

    MASTER_DICT[2] = {
        1: first_class,
        2: second_class,
        3: third_class
    }

    #crude classification of age groups, but not sure how differentiating pre-school vs school age would have an affect
    toddler = np.where((data[:,5] > 0) & (data[:,5] <= 3.0))[0]
    child = np.where((data[:,5] > 3.0) & (data[:,5] <= 12.0))[0]
    teen = np.where((data[:,5] > 12.0) & (data[:,5] <= 18.0))[0]
    young_adult = np.where((data[:,5] > 18.0) & (data[:,5] <= 39.0))[0]
    middle_aged = np.where((data[:,5] > 39.0) & (data[:,5] <= 64.0))[0]
    elderly = np.where((data[:,5] > 64.0))[0]


    MASTER_DICT[5] = {
        3: toddler,
        12: child,
        18: teen,
        39: young_adult,
        64: middle_aged,
        200: elderly
    }



    cabinA = np.where(data[:,10] == 65)[0]
    cabinB = np.where(data[:,10] == 66)[0]
    cabinC = np.where(data[:,10] == 67)[0]
    cabinD = np.where(data[:,10] == 68)[0]
    cabinE = np.where(data[:,10] == 69)[0]
    cabinF = np.where(data[:,10] == 70)[0]
    cabinG = np.where(data[:,10] == 71)[0]

    MASTER_DICT[10] = {
        65: cabinA,
        66: cabinB,
        67: cabinC,
        68: cabinD,
        69: cabinE,
        70: cabinF,
        71: cabinG
    }
    return MASTER_DICT;


data = np.asarray(getData(os.path.join(os.path.dirname(__file__), 'train.csv')))

survivors = np.where(data[:,1] == 1)[0]

MASTER_DICT = getListOfAttributesAndNumberOfPeopleWithThatAttribute(data)

prediction_file = open(os.path.join(os.path.dirname(__file__), 'predict_test.csv'), 'w')

predicted_data = []

test_data = np.asarray(getData(os.path.join(os.path.dirname(__file__), 'test.csv'), False))
for passengerToEvaluate in test_data:
    arrayToReduce = []

    for index, attribute in enumerate(passengerToEvaluate):
        index = index + 1
        if isinstance(MASTER_DICT[index], dict):
            if index is 5: #index 5 is age, so we use <= instead of -1
                if attribute is not -1:
                    for k, v in MASTER_DICT[index].items():
                        if passengerToEvaluate[index] <= k:
                            #print('For', passengerToEvaluate[2] , 'Adding index', index, 'with attribute', attribute)
                            arrayToReduce.append(v)
                            break
            else:
                if attribute is not -1:
                    #print('For', passengerToEvaluate[2] , 'Adding index', index, 'with attribute', attribute)
                    arrayToReduce.append(MASTER_DICT[index][attribute])

    if len(reduce(np.intersect1d, arrayToReduce)) is 0:
        arrayToReduce.pop()
    survival_chance = len(reduce(np.intersect1d, arrayToReduce + [survivors])) / len(reduce(np.intersect1d, arrayToReduce))

    print(passengerToEvaluate[2], ' has a ', survival_chance * 100, '% chance of survival')
    prediction_file.write(str(passengerToEvaluate[0]) + ',' + ('1' if survival_chance > .75 else '0') + "\n")
    predicted_data.append([passengerToEvaluate[0], survival_chance*100])
prediction_file.close()

predicted_data = np.asarray(predicted_data, dtype=int)

# The below will only work if we use the training data to precit. With the test data we can't know if we predicted correctly or not

# predicted_survivors_indices = np.where(predicted_data[:,1] > 75)[0]

# number_right = 0
# number_wrong = 0

# for index in predicted_survivors_indices:
#     if data[index][1] == 1:
#         number_right += 1
#     else:
#         number_wrong += 1


# #print(number_right)
# # print(number_wrong)
# print(number_right / len(survivors) * 100)

# print(len(predicted_survivors_indices) / len(data) *100)

# print(len(survivors) / len(data) * 100)
sys.exit(0)
