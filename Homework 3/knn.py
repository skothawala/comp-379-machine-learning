import math, operator

#modeled somewhat (with optimizations) from https://machinelearningmastery.com/tutorial-to-implement-k-nearest-neighbors-in-python-from-scratch/ 
class KNearestNeighbors(object):
	def __init__(self, trainingData, trainingDataLabels, k = 1):
		self.trainingData = trainingData
		self.trainingDataLabels = trainingDataLabels
		self.k = k
	
	def getTrainingData(self):
		return self.trainingData
	
	def getDistance(self, dataPointA, dataPointB):
		distance = 0
		for x in range(len(dataPointA)):
			distance += pow((dataPointA[x] - dataPointB[x]), 2)
		return math.sqrt(distance)
	
	def getNeigbors(self, toPredict):
		distances = []
		for x in range(len(self.trainingData)):
			dist = self.getDistance(toPredict, self.trainingData[x])
			distances.append((x, dist))
		distances.sort(key=operator.itemgetter(1))
		neighbors = []
		for x in range(self.k):
			neighbors.append(distances[x][0])
		return neighbors

	def predict(self, toPredict):
		neighbors = self.getNeigbors(toPredict)
		labelCounts = {}
		for x in neighbors:
			label = self.trainingDataLabels[x]
			if label in labelCounts:
				labelCounts[label] += 1
			else:
				labelCounts[label] = 1
		sortedCounts = sorted(labelCounts.items(), key=operator.itemgetter(1), reverse=True)
		return sortedCounts[0][0]