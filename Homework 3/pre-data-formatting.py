#######THIS splits titanic dataset into 3 normalized csv files. 1) Training 2) Dev and 3) Test
import numpy as np
import sys, re, os
from functools import reduce
assert sys.version_info >= (3,0) #Running with 2.xx does work, but let's use 3, shall we


"""
Returns an array with the list of passengers and their attributes. Modified from earlier version

Params:
`fileLocation` - location of the file to read from
`isTraining` - if true, then the survivor column is empty and indexes need to be moved down one
"""    
def getData(fileLocation, isTraining = True):

    passengers = [] #to return
    with open(fileLocation, 'r') as f:
        f.readline() #get rid of headers
        for line in f:
            line = re.sub(r'(\"(.*),(.*)\")', r"\3 \2", line) #format lName, fName -> fName lName
            line = line[:-1].split(',') #get rid of \n and explode to csv

            passenger = np.empty((13 if isTraining else 12), dtype=object) #create holder array
            removePassenger = False
            
            for index, passengerAttribute in enumerate(line):
                if index is 1 and isTraining:
                    passenger[index] = -1 if int(passengerAttribute) is 0 else 1
                elif index is (4 if isTraining else 3):
                    if passengerAttribute ==  'male':
                        passenger[index] = 1
                    else:
                        passenger[index] = 2
                elif index is (5 if isTraining else 4):
                    #some ages are to .5's
                    if passengerAttribute is not '':
                        passenger[index] = np.float64(passengerAttribute)
                    else:
                        removePassenger = True
                        break
                elif index in ([0, 2, 6, 7] if isTraining else [0, 5, 6]):
                    #these are boolean values we might want to run correlations on
                    if passengerAttribute is not '':
                        passenger[index] = int(passengerAttribute)
                elif index is (9 if isTraining else 8):
                    if passengerAttribute is not '' and np.float64(passengerAttribute) > 0:
                        passenger[index] = np.float64(passengerAttribute)
                    else:
                        removePassenger = True
                        break
                elif index is (10 if isTraining else 9):
                    if passengerAttribute != '':
                        passenger[index] = ord(passengerAttribute[0])
                    else:
                        passenger[index] = 0
                else:
                    passenger[index] = passengerAttribute
            if removePassenger is False:
                passengers.append(passenger)             
    return passengers;

data = np.asarray(getData(os.path.join('train.csv')))


np.random.shuffle(data)

# indexToStop = round(len(data)*.7)
# trainingDataSet = data[:indexToStop,:]
# testDataSet = data[indexToStop:,:]

genderAndAge = data[:, [2,5,4,10]].astype(np.float64)
labels = data[:, [1]].flatten().astype(int)
 
STD_genderAndAge = np.copy(genderAndAge)                                                            
STD_genderAndAge[:, 0] = (genderAndAge[:, 0] - genderAndAge[:, 0].mean()) / genderAndAge[:, 0].std()
STD_genderAndAge[:, 1] = (genderAndAge[:, 1] - genderAndAge[:, 1].mean()) / genderAndAge[:, 1].std()
STD_genderAndAge[:, 2] = (genderAndAge[:, 2] - genderAndAge[:, 2].mean()) / genderAndAge[:, 2].std()
STD_genderAndAge[:, 3] = (genderAndAge[:, 3] - genderAndAge[:, 3].mean()) / genderAndAge[:, 3].std()


endIndexTrainingData = round(len(data)*.7)
trainingDataSet = STD_genderAndAge[:endIndexTrainingData,:]
labelsTraining = labels[:endIndexTrainingData]
endIndexDevData = endIndexTrainingData + round(len(data)*.15)
devDataSet = STD_genderAndAge[endIndexTrainingData:endIndexDevData,:]
labelsDev = labels[endIndexTrainingData:endIndexDevData]
testDataSet = STD_genderAndAge[endIndexDevData:,:]
labelsTest = labels[endIndexDevData:]

np.savetxt('normalized_training.txt', trainingDataSet)

np.savetxt('normalized_training_labels.txt', labelsTraining)

np.savetxt('normalized_dev.txt', devDataSet)

np.savetxt('normalized_dev_labels.txt', labelsDev)

np.savetxt('normalized_test.txt', testDataSet)

np.savetxt('normalized_test_labels.txt', labelsTest)
