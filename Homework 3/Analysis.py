import numpy as np
import sys
from sklearn.svm import SVC, LinearSVC
import sklearn
from knn import KNearestNeighbors
assert sys.version_info >= (3,0) #Running with 2.xx does work, but let's use 3, shall we


########run pre-data-formatiing to get formatted data

trainingDataSet = np.loadtxt('normalized_training.txt')
labelsTraining = np.loadtxt('normalized_training_labels.txt')
devDataSet = np.loadtxt('normalized_dev.txt')
labelsDev = np.loadtxt('normalized_dev_labels.txt')
testDataSet = np.loadtxt('normalized_test.txt')
labelsTest = np.loadtxt('normalized_test_labels.txt')

svm = LinearSVC()
svm.fit(trainingDataSet, labelsTraining)

numberAccurate = 0
for idx, i in enumerate(devDataSet):
    if svm.predict([i]) == labelsDev[idx]:
        numberAccurate += 1

accuracyScore = numberAccurate / len(labelsDev) * 100
print("Default Accuracy: " + str(accuracyScore) + "%")
print("Default F1: " + str(sklearn.metrics.f1_score(svm.predict(devDataSet), labelsDev)))

svmOptimized = LinearSVC(C=10)
svmOptimized.fit(trainingDataSet, labelsTraining)

numberAccurate = 0
for idx, i in enumerate(devDataSet):
    if svmOptimized.predict([i]) == labelsDev[idx]:
        numberAccurate += 1

accuracyScore = numberAccurate / len(labelsDev) * 100

print("C=100 Accuracy: " + str(accuracyScore) + "%")
print("C=100 F1: " + str(sklearn.metrics.f1_score(svmOptimized.predict(devDataSet), labelsDev)))



k = KNearestNeighbors(trainingDataSet, labelsTraining)
numberAccurate = 0
for idx, i in enumerate(devDataSet):
    if k.predict(i) == labelsDev[idx]:
        numberAccurate += 1

accuracyScore = numberAccurate / len(labelsDev) * 100

print("KNN Accuracy (K=1): " + str(accuracyScore) + "%")


k = KNearestNeighbors(trainingDataSet, labelsTraining, 5)
numberAccurate = 0
for idx, i in enumerate(devDataSet):
    if k.predict(i) == labelsDev[idx]:
        numberAccurate += 1

accuracyScore = numberAccurate / len(labelsDev) * 100

print("KNN Accuracy (K=20): " + str(accuracyScore) + "%")


print("\n##########TEST DATA SET##########\n")

numberAccurate = 0
for idx, i in enumerate(testDataSet):
    if svmOptimized.predict([i]) == labelsTest[idx]:
        numberAccurate += 1

accuracyScore = numberAccurate / len(labelsTest) * 100

print("SVM TEST Accuracy: " + str(accuracyScore) + "%")
print("SVM TEST F1: " + str(sklearn.metrics.f1_score(svmOptimized.predict(testDataSet), labelsTest)))


numberAccurate = 0
for idx, i in enumerate(testDataSet):
    if k.predict(i) == labelsTest[idx]:
        numberAccurate += 1

accuracyScore = numberAccurate / len(labelsTest) * 100
print("KNN Accuracy TEST: " + str(accuracyScore) + "%")



