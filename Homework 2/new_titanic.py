import numpy as np
import sys, re, os
from functools import reduce
from adaline import AdalineGD
import matplotlib.pyplot as plt

assert sys.version_info >= (3,0) #Running with 2.xx does work, but let's use 3, shall we



"""
Returns an array with the list of passengers and their attributes. Modified from earlier version

Params:
`fileLocation` - location of the file to read from
`isTraining` - if true, then the survivor column is empty and indexes need to be moved down one
"""    
def getData(fileLocation, isTraining = True):

    passengers = [] #to return
    with open(fileLocation, 'r') as f:
        f.readline() #get rid of headers
        for line in f:
            line = re.sub(r'(\"(.*),(.*)\")', r"\3 \2", line) #format lName, fName -> fName lName
            line = line[:-1].split(',') #get rid of \n and explode to csv

            passenger = np.empty((13 if isTraining else 12), dtype=object) #create holder array
            removePassenger = False
            
            for index, passengerAttribute in enumerate(line):
                if index is 1 and isTraining:
                    passenger[index] = -1 if int(passengerAttribute) is 0 else 1
                elif index is (4 if isTraining else 3):
                    if passengerAttribute ==  'male':
                        passenger[index] = 1
                    else:
                        passenger[index] = 2
                elif index is (5 if isTraining else 4):
                    #some ages are to .5's
                    if passengerAttribute is not '':
                        passenger[index] = np.float64(passengerAttribute)
                    else:
                        removePassenger = True
                        break
                elif index in ([0, 2, 6, 7] if isTraining else [0, 5, 6]):
                    #these are boolean values we might want to run correlations on
                    if passengerAttribute is not '':
                        passenger[index] = int(passengerAttribute)
                elif index is (9 if isTraining else 8):
                    if passengerAttribute is not '' and np.float64(passengerAttribute) > 0:
                        passenger[index] = np.float64(passengerAttribute)
                    else:
                        removePassenger = True
                        break
                elif index is (10 if isTraining else 9):
                    if passengerAttribute != '':
                        passenger[index] = ord(passengerAttribute[0])
                    else:
                        passenger[index] = 0
                else:
                    passenger[index] = passengerAttribute
            if removePassenger is False:
                passengers.append(passenger)             
    return passengers;

data = np.asarray(getData(os.path.join('train.csv')))


# ada = AdalineSGD(n_iter=100, eta=0.001, random_state=1)
# genderAndAge = data[:, [5, 4]].astype(float)
# labels = data[:, [1]].flatten().astype(int)
# ada.fit(genderAndAge, labels)


np.random.shuffle(data)

indexToStop = round(len(data)*.7)
trainingDataSet = data[:indexToStop,:]
testDataSet = data[indexToStop:,:]

genderAndAge = trainingDataSet[:, [2,5,4,10]].astype(np.float64)
labels = trainingDataSet[:, [1]].flatten().astype(int)
 
STD_genderAndAge = np.copy(genderAndAge)                                                            
STD_genderAndAge[:, 0] = (genderAndAge[:, 0] - genderAndAge[:, 0].mean()) / genderAndAge[:, 0].std()
STD_genderAndAge[:, 1] = (genderAndAge[:, 1] - genderAndAge[:, 1].mean()) / genderAndAge[:, 1].std()
STD_genderAndAge[:, 2] = (genderAndAge[:, 2] - genderAndAge[:, 2].mean()) / genderAndAge[:, 2].std()
STD_genderAndAge[:, 3] = (genderAndAge[:, 3] - genderAndAge[:, 3].mean()) / genderAndAge[:, 3].std()

ada = None
ada = AdalineGD(n_iter=10000, eta=0.000001)                                                             
ada.fit(STD_genderAndAge, labels)                           

testGenderAndAge = testDataSet[:, [2,5,4,10]].astype(np.float64)
testLabelsActual = testDataSet[:, [1]].flatten().astype(int)
numberRight = 0
for idx, testPassenger in enumerate(testGenderAndAge):
    prediction = ada.predict(testPassenger)
    if prediction == testLabelsActual[idx]:
        numberRight += 1
print(str((numberRight)/len(testGenderAndAge)*100))


plt.clf()                                                                                           
plt.cla()                                                   
plt.close()                                           
plt.plot(range(901, len(ada.cost_) + 1), ada.cost_[-100:], marker='o')                                    
plt.xlabel('Epochs')
plt.ylabel('Sum-squared-error')
plt.tight_layout()
plt.savefig('./error_graph_inset.png', dpi=300)

plt.clf()                                                                                           
plt.cla()                                                   
plt.close()                                           
plt.plot(range(1, len(ada.cost_) + 1), ada.cost_, marker='o')                                    
plt.xlabel('Epochs')
plt.ylabel('Sum-squared-error')
plt.tight_layout()
plt.savefig('./error_graph_all.png', dpi=300)

#70% with ada = AdalineGD(n_iter=10000, eta=0.000001)