import numpy
import csv

def predict(sample, weights):
	z = 0
	for idx, feature in enumerate(sample):
		z += weights[idx] * float(feature)
	return 1 if z >= 0 else 0

def runPerceptron(inputTrainingSet, weights, learningRate):
	newWeights = weights
	for sample in inputTrainingSet:
		predictedLabel = predict(sample[:-1], newWeights)
		actualLabel = float(sample[-1])
		for idx, weight in enumerate(newWeights):
			#if learningRate * (actualLabel - predictedLabel) * int(sample[idx]) > 0:
				#print (str(sample[0]) + "\t" + str(sample[1]) + "\t", str(learningRate * (actualLabel - predictedLabel) * int(sample[idx])))
			newWeights[idx] = newWeights[idx] + (learningRate * (actualLabel - predictedLabel) * float(sample[idx]))
	return newWeights


# with open('linearly_separable_dataset.csv', 'r') as f:
# 	reader = csv.reader(f)
# 	dataset = list(reader)


# weights = [0, 0]
# allWeights = []
# allWeights.append(weights)
# for i in range(0, 1000):
# 	#print("Running for " + str(i) + "th time")
# 	weights = runPerceptron(dataset[1:], weights, 0.1)
# 	print(weights[0])



# print("64 inches tall, 210 lbs; Expected: 1, Predicted: " + str(predict([64, 210], weights)))
# print("64 inches tall, 130 lbs; Expected: 0, Predicted: " + str(predict([64, 130], weights)))

# print("60 inches tall, 210 lbs; Expected: 1, Predicted: " + str(predict([60, 210], weights)))
# print("60 inches tall, 170 lbs; Expected: 1, Predicted: " + str(predict([60, 170], weights)))
# print("60 inches tall, 110 lbs; Expected: 0, Predicted: " + str(predict([60, 110], weights)))

with open('non_linearly_separable_dataset.csv', 'r') as f:
	reader = csv.reader(f)
	dataset = list(reader)


weights = [0, 0]
allWeights = []
allWeights.append(weights)
for i in range(0, 1000):
	#print("Running for " + str(i) + "th time")
	weights = runPerceptron(dataset[1:], weights, 0.1)
	print(weights[0])


